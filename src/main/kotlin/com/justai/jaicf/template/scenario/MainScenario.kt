package com.justai.jaicf.template.scenario

import com.justai.jaicf.activator.caila.caila
import com.justai.jaicf.activator.regex.regex
import com.justai.jaicf.model.scenario.Scenario
import net.de1mos.jaicf.Config
import net.de1mos.jaicf.models.UserStorage

object MainScenario : Scenario() {

    init {
        state("Start") {
            globalActivators {
                regex("/start")
            }
            action {
                reactions.say("Начнём и очень хорошо8!!!!!! КЕК")
            }
        }

        state("save value") {
            globalActivators {
                regex("сохрани(?<val>.*)")
            }

            action {
                val group = activator.regex?.group("val")?.trim()
                reactions.say("Ща как сохраню '$group'")
                Config.save(UserStorage(context.clientId, group!!))
            }
        }

        state("remind me") {
            globalActivators {
                regex("напомни")
            }

            action {
                val value = Config.get(context.clientId)
                if (value != null) {
                    reactions.say("ты сохранял '${value.data}'")
                } else {
                    reactions.say("Да я тоже ничего не помню")
                }
            }
        }
        
        state("check") {
            globalActivators {
                regex("test")
            }
            action {
                reactions.say("v10")
            }
        }

        state("chipper") {
            globalActivators {
                intent("Whatscheaper")
            }

            action {
                val slots = activator.caila?.slots!!
                val priceFirst = slots["cost_first"]!!.toDouble() / slots["amount_first"]!!.toDouble()
                val priceSecond = slots["cost_second"]!!.toDouble() / slots["amount_second"]!!.toDouble()
                val text = "Выгоднее ${if (priceFirst > priceSecond) "Первый" else "Второй"}\n " +
                        "Стоимость первого за единицу ${"%.2f".format(priceFirst)}, второго ${"%.2f".format(priceSecond)}"
                reactions.say(text)
            }
        }

        state("c1") {
            globalActivators {
                intent("wc_start")
            }

            action {
                reactions.say("Какой первый товар?")
                context.session["first_amount"] = null
                context.session["first_cost"] = null
                context.session["second_amount"] = null
                context.session["second_cost"] = null
            }


        }

        state("goods slot") {
            activators {
                intent("wc1")
            }

            action {
                val slots = activator.caila?.slots!!
                if (context.session["first_amount"] == null) {
                    context.session["first_amount"] = slots["amount"]
                    context.session["first_cost"] = slots["cost"]
                } else {
                    context.session["second_amount"] = slots["amount"]
                    context.session["second_cost"] = slots["cost"]
                }

                if (context.session["second_amount"] == null) {
                    reactions.say("Какой второй товар")
                } else {
                    val priceFirst = context.session["first_cost"]!!.toString()
                        .toDouble() / context.session["first_amount"]!!.toString().toDouble()
                    val priceSecond = context.session["second_cost"]!!.toString()
                        .toDouble() / context.session["second_amount"]!!.toString().toDouble()
                    val text = "Выгоднее ${if (priceFirst < priceSecond) "Первый" else "Второй"}\n " +
                            "Стоимость первого за единицу ${"%.2f".format(priceFirst)}, " +
                            "второго ${"%.2f".format(priceSecond)}"
                    reactions.say(text)
                }
            }
        }

        state("Hello") {
            activators {
                intent("Hello")
            }

            action {
                reactions.say("Привет чувак2!")
            }
        }

        state("Bye") {
            activators {
                intent("Bye")
            }

            action {
                reactions.say("Скоро увидимся!")
            }
        }

        fallback {
            reactions.say("Мне пока нечего сказать...")
        }
    }
}