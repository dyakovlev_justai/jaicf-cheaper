package net.de1mos.jaicf.models

data class UserStorage(
    val _id: String,
    val data: String
)