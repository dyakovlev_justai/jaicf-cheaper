package net.de1mos.jaicf

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.justai.jaicf.activator.caila.CailaNLUSettings
import com.mongodb.MongoClient
import com.mongodb.MongoClientURI
import com.mongodb.client.MongoCollection
import com.mongodb.client.model.Filters
import com.mongodb.client.model.UpdateOptions
import net.de1mos.jaicf.models.UserStorage
import org.bson.Document
import java.util.*

object Config {
    private val mongo: MongoCollection<Document>
    private val mapper: ObjectMapper = jacksonObjectMapper()

    init {

        val mongoUrl = Properties().run {
            load(CailaNLUSettings::class.java.getResourceAsStream("/jaicp.properties"))
            getProperty("mongoUrl")
        }

        val uri = MongoClientURI(String(Base64.getDecoder().decode(mongoUrl)))
        val client = MongoClient(uri)

        mongo = client.getDatabase(uri.database!!)
            .getCollection("user_remember_storage")
    }

    fun save(record: UserStorage) {
        val doc = Document.parse(mapper.writeValueAsString(record))
        mongo.replaceOne(Filters.eq("_id", record._id), doc, UpdateOptions().upsert(true))
    }

    fun get(id: String): UserStorage? {
        val first = mongo.find(Filters.eq("_id", id)).first()
        return if (first != null) {
            mapper.readValue(first.toJson(), UserStorage::class.java)
        } else {
            null
        }
    }
}